### Что есть
Дизайнер предоставил дизайн главной страницы web-приложения в виде [sketch-файла](https://sketchviewer.com/sketches/5d72412156286d0012563946/latest/). Он содержит только iPad версию. Она достаточно хорошо подходит для Desktop, но мобильную версию нужно додумать разработчику.

### Что сделать
- Cверстать страницу в виде React+Redux приожения: desktop + mobile версии.
- Все компоненты на странице — интерактивные и меняют своё состояние при наведении курсора, клике, итд.
- При клике на `Load more` в компоненте `Activity` догружается часть данных с какого-нибудь Fake API сервера, например [mocky.io](https://www.mocky.io)
- При перемещении курсора по компонентам графиков данные также меняются.
- Компонент выбора даты также интерактивный и позволяет выбрать разные временные промежутки.
- При клике на иконку пользователя появляется выпадающее меню.
- При нажатии на зеленую кнопку `+` можно добавить пункты в левое меню. Название нового пункта нужно спросить у пользователя. 
- Добавленные пункты левого меню сохраняются после перезагрузки страницы

### Как сделать
Перед началом работы нужно оценить задачу и рассказать эстимейт. Исходные коды нужно залить на Github или Bitbucket, а чтобы посмотреть на live-версию присылать ссылку на готовую страницу.
