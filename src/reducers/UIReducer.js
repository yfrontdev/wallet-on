import { MOBILE_MENU_TOGGLE } from "../actions/types";

const initialState = {
  isOpenMobileMenu: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MOBILE_MENU_TOGGLE:
      return {
        ...state,
        isOpenMobileMenu: !state.isOpenMobileMenu
      };

    default:
      return state;
  }
};
