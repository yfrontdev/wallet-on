import { ACTIVITIES_FETCH, ACTIVITIES_INIT } from "../actions/types";

const initialState = {
  isFetching: false,
  isRejected: false,
  items: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case `${ACTIVITIES_INIT}_FULFILLED`:
      return {
        ...state,
        items: [...action.payload],
        isFetching: false
      };

    case `${ACTIVITIES_FETCH}_PENDING`:
      return { ...state, isFetching: true };

    case `${ACTIVITIES_FETCH}_FULFILLED`:
      return {
        ...state,
        items: [...state.items, ...action.payload],
        isFetching: false
      };
      
    case `${ACTIVITIES_FETCH}_REJECTED`:
      return { ...state, isRejected: true, isFetching: false };

    default:
      return state;
  }
};
