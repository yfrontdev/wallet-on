import {
  RECIPIENTS_FETCH
} from "../actions/types";

const initialState = {
  isFetching: false,
  isRejected: false,
  total: 12,
  items: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case `${RECIPIENTS_FETCH}_PENDING`:
      return { ...state, isFetching: true };
    case `${RECIPIENTS_FETCH}_FULFILLED`:
      return { ...state, items: [...action.payload], isFetching: false };
    case `${RECIPIENTS_FETCH}_REJECTED`:
      return { ...state, isRejected: true, isFetching: false };

    default:
      return state;
  }
};
