import { BALANCES_FETCH } from "../actions/types";

const initialState = {
  items: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case `${BALANCES_FETCH}_FULFILLED`:
      return { ...state, items: [...action.payload] };

    default:
      return state;
  }
};
