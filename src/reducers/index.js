import { combineReducers } from 'redux';
import SiteReducer from './SiteReducer';
import UIReducer from './UIReducer';
import RecipientsReducer from './RecipientsReducer';
import ActivitiesReducer from './ActivitiesReducer';
import BalancesReducer from './BalancesReducer';

export default combineReducers({
  site: SiteReducer,
  ui: UIReducer,
  recipients: RecipientsReducer,
  activities: ActivitiesReducer,
  balances: BalancesReducer
});