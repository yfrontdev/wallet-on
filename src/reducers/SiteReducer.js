import {
  CUSTOM_MENU_FETCH,
  ADD_CUSTOM_MENU_ITEM
} from '../actions/types';

const initialState = {
  customMenu: [],
  notifications: {
    activity: 2
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CUSTOM_MENU_FETCH:
      return { ...state, customMenu: [...action.payload] };

    case ADD_CUSTOM_MENU_ITEM:
      return { ...state, customMenu: [...state.customMenu, action.payload] };

    default:
      return state;
  }
};
