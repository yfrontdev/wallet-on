import React from 'react';
import classNames from 'classnames';
import { Icon } from 'components';

const Avatar = ({ img, icon, className, hover=true, size, onClick }) => {
  const styles = img ? { backgroundImage: `url(${img})` } : {};
  const ico = icon ? <Icon name={icon} /> : null;
  const classes = classNames(
    "avatar",
    {
      "cursor-default": !hover,
      [`avatar-size-${size}`]: size
    },
    className
  );

  return (
    <div className={classes} style={styles} onClick={onClick}>
      {ico}
    </div>
  );
};

export default Avatar;