import React, { useState } from 'react';
import Select, { components } from "react-select";
import { Icon } from 'components';

const CustomSelect = ({ icon = null, onChange }) => {
  const options = [
    { value: "week", label: "Current week" },
    { value: "month", label: "Current month" },
    { value: "year", label: "Current year" }
  ];

  const [selectedOption, setSelectedOption] = useState(options[0]);
  const [isOpen, toggleOpen] = useState(false);

  const changeHadler = (val) => {
    setSelectedOption(val);
    onChange(val.value);
  }

  /**-------------------------------
   * Custom DropdownIndicator
   *-----------------------------*/
  const DropdownIndicator = (props) => (
    <components.DropdownIndicator {...props}>
      <Icon name="arrow-down" />
    </components.DropdownIndicator>
  );

  return (
    <div className="select-period">
      {icon && <Icon name={icon} />}
      <Select
        className="custom-select"
        classNamePrefix="custom-select"
        value={selectedOption}
        onChange={changeHadler}
        options={options}
        isSearchable={false}
        isClearable={false}
        menuIsOpen={isOpen}
        components={{
          IndicatorSeparator: null,
          DropdownIndicator
        }}
        onMenuOpen={() => {
          toggleOpen(true);
        }}
        onMenuClose={() => {
          toggleOpen(false);
        }}
      />
    </div>
  );
};

export default CustomSelect;