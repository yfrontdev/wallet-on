import React from 'react';

const TitlePage = ({ children }) => {
  return (
    <div className="title-page">
      {children}
    </div>
  );
};

export default TitlePage;