import React from 'react';

const TitleBlock = ({ children }) => {
  return (
    <div className="title-block">
      {children}
    </div>
  );
};

export default TitleBlock;