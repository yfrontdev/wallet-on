import React from 'react';
import classNames from "classnames";

const Badge = ({ count, overflowCount = "99", dot = false }) => {
  const getNumberedDispayCount = () => {
    const displayCount = (count > overflowCount) ? `${overflowCount}+` : count;
    return displayCount;
  }
  
  const getDispayCount = () => {
    if (dot) {
      return '';
    }
    return getNumberedDispayCount();
  }

  return (
    <div className={classNames("badge", {
      'has-dot': dot
    })}>
      { getDispayCount() }
    </div>
  );
};

export default Badge;