import React from 'react';

const LinkBase = (props) => {
  return (
    <div className="link-base" {...props}>
      {props.children}
    </div>
  );
};

export default LinkBase;