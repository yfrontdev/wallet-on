import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name='empty', type='ui', className='', size=[16, 16] }) => {
  return (
    <Fragment>
      <svg
        width={size[0]}
        height={size[1]}
        viewBox={`0 0 ${size[0]} ${size[1]}`}
        className={`icon icon-${name} ${className}`}
      >
        <use xlinkHref={`#${type}--${name}`} />
      </svg>
    </Fragment>
  );
};

Icon.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  size: PropTypes.arrayOf(PropTypes.number)
};

export default Icon;
