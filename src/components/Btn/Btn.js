import React from 'react';
import classNames from 'classnames';
import { Spinner } from 'components';

const Btn = ({ onClick, loading, children }) => {
  const spinner = loading ? <Spinner /> : null;
  return (
    <button
      className={classNames("btn", {
        active: loading
      })}
      type="button"
      onClick={onClick}
    >
      <span>
        {spinner}
        {children}
      </span>
    </button>
  );
};

export default Btn;