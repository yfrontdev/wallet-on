import React from 'react';
import { AreaChart, Area, ResponsiveContainer, Tooltip } from "recharts";

const CustomTooltip = ({ active, payload }) => {
  if (active) {
    return (
      <div className="custom-tooltip">
        <p className="label">
          {payload[0].value} 
          <span className="currency">EUR</span>
        </p>
      </div>
    );
  }

  return null;
};

const SimpleAreaChart = ({ data, id, color = "#5187E0" }) => {
  return (
    <ResponsiveContainer width="100%" height={80} id={`chart-${id}`}>
      <AreaChart data={data} margin={{ right: 10, top: 10 }}>
        <defs>
          <linearGradient id={`colorUv-${id}`} x1="0" y1="0" x2="1" y2="0">
            <stop offset="0%" stopColor={color} stopOpacity={0.13} />
            <stop offset="95%" stopColor={color} stopOpacity={0.01} />
          </linearGradient>
        </defs>
        <Tooltip active={true} cursor={false} content={<CustomTooltip />} />
        <Area
          type="monotone"
          dataKey="uv"
          stroke={color}
          strokeWidth="3px"
          fillOpacity={1}
          fill={`url(#colorUv-${id}`}
          activeDot={{ r: 5, fill: "#fff", stroke: color, strokeWidth: 3 }}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default SimpleAreaChart;