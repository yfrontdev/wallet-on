import React from 'react';
import classNames from 'classnames';

const Burger = ({ onClick, isOpen }) => {
  return (
    <div
      className={classNames("burger", {
        "is-open": isOpen
      })}
      onClick={onClick}
    >
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};

export default Burger;