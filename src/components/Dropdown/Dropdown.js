import React, { useState } from 'react';
import { CSSTransition } from "react-transition-group";

const Dropdown = ({ overlay, className="", children }) => {
  const [isOpen, toggleIsOpen] = useState(false);

  const handleClickDocument = (e) => {
    document.removeEventListener("click", handleClickDocument);
    toggleIsOpen(false);
  }

  const handleToggle = () => {
    if (!isOpen) document.addEventListener('click', handleClickDocument);
    toggleIsOpen(!isOpen);
  }

  return (
    <div className={`dropdown ${className}`}>
      <div className="dropdown-header" onClick={() => {handleToggle()}}>
        {children}
      </div>
        <CSSTransition
        in={isOpen}
        timeout={350}
        unmountOnExit
        classNames="fade-down"
      >
        <div className="dropdown-body">
          {overlay}
        </div>
      </CSSTransition>
    </div>
  );
};

export default Dropdown;