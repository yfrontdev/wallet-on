import React from 'react';
import classNames from "classnames";
import { Avatar } from 'components';

const TitleBlock = ({ name, icon, status, date, price }) => {
  return (
    <div className="activity-item">
      <div className="activity-item-inner">
        <Avatar
          className="activity-item-avatar"
          icon={icon}
          hover={false}
          size="md"
        />
        <div className="activity-item-content">
          <div className="activity-item-title">{name}</div>
          <div className="activity-item-status">
            <span
              className={`activity-item-status-value ${status.toLowerCase()}`}
            >
              {status}
            </span>
            <span className="activity-item-status-date">{date}</span>
          </div>
        </div>
        <div className="activity-item-value">
          <div
            className={classNames("activity-item-value-main",
              {
                'completed': price.status === 'positive',
                'error': price.status === 'negative'
              }
            )}
          >
            {price.value}
          </div>
          <div className="activity-item-value-secondary">{price.convert}</div>
        </div>
      </div>
    </div>
  );
};

export default TitleBlock;