import React from 'react';
import { Link } from 'react-router-dom';
import logo from "static/img/svg/logo.svg";

const AppLogo = () => {
  return (
    <Link to="/" className="app-logo">
      <img src={logo} alt="Wallet.on" />
    </Link>
  );
};

export default AppLogo;