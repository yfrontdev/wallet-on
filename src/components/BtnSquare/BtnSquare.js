import React from 'react';
import classNames from "classnames";
import { Icon, Badge } from 'components';

const BtnSquare = ({ icon, type="default", className, styles={}, onClick, badge=null, children }) => {
  const ico = icon ? <Icon name={icon} /> : null;
  const classes = classNames(
    "btn-square", `btn-square--${type}`,
    {
      "primary": type
    },
    className
  );
  const isBadge = badge ? <Badge count={badge} /> : null;

  return (
    <button type="button" className={classes} style={styles} onClick={onClick}>
      {ico}
      {children}
      {isBadge}
    </button>
  );
};

export default BtnSquare;