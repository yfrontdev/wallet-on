import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { Wrapper } from 'containers';
import { PageOverview } from "pages";

const RoutesWrapper = ({ location }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);
  
  return (
    <TransitionGroup component={null}>
      <CSSTransition
        timeout={300}
        classNames="transitionpage"
        key={location.key}
      >
        <Switch>
          <Route exact path="/" component={PageOverview} />
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  );
};

const Routes = () => (
  <Wrapper>
    <Route render={({ location }) => (
      <RoutesWrapper location={location} />
    )} />
  </Wrapper>
);

export default Routes;
