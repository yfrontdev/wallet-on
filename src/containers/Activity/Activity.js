import React, { useEffect } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { useSelector, useDispatch } from "react-redux";
import { Card, TitleBlock, ActivityItem, Btn } from 'components';
import { activitiesInit, activitiesFetch } from "actions";

const Activity = () => {
  const activities = useSelector(state => state.activities.items);
  const isFetching = useSelector(state => state.activities.isFetching);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(activitiesInit());
  }, [dispatch]);

  return (
    <div className="activity">
      <Card>
        <TitleBlock>Activity</TitleBlock>
        <TransitionGroup className="activity-list">
          {activities.map((activity, idx) => (
            <CSSTransition
              key={activity.id + idx}
              timeout={400}
              classNames="fade"
            >
              <ActivityItem {...activity} key={activity.id + idx} />
            </CSSTransition>
          ))}
        </TransitionGroup>
        <Btn
          onClick={() => {
            dispatch(activitiesFetch());
          }}
          loading={isFetching}
        >
          Load more
        </Btn>
      </Card>
    </div>
  );
};

export default Activity;