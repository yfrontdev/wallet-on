import React, { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import classNames from 'classnames';
import { Header, Sidebar } from 'containers';

const Wrapper = (props) => {
  useEffect(() => {
    fetch('/static/sprite/sprite.svg')
      .then(r => r.text())
      .then(sprite => {
        document.querySelector("#sprite").innerHTML = sprite;
      })
  }, [])

  return (
    <div
      className={classNames('app', {
        // 'is-touch': isMobile,
      })}
    >
      <Router>
        <Header />
        <Sidebar />

        <main className="main">
          {props.children}
        </main>

      </Router>
    </div>
  );
}

export default Wrapper;