import React, { useEffect } from 'react';
import classNames from 'classnames';
import { TitleBlock, Card, SimpleAreaChart, CustomSelect } from "components";
import { useSelector, useDispatch } from 'react-redux';
import { balancesFetch, balancesUpdate } from "actions";

const BalancesCard = ({ name, total, diff, data, period, colorCharts, id }) => {
  return (
    <div className="balances-card">
      <Card>
        <div className="balances-card-content">
          <div className="balances-card-name">{name}</div>
          <div className="balances-card-values">
            <div className="balances-card-total">
              <span className="balances-card-total-value">{total.value}</span>
              <span className="balances-card-total-currency">{total.currency}</span>
            </div>
            <div className="balances-card-diff">
              <div
                className={classNames("balances-card-diff-value", {
                  negative: +diff < 0,
                  positive: +diff > 0
                })}
              >
                {diff > 0 ? `+${diff}` : diff}%
              </div>
              <div className="balances-card-diff-period">{period}</div>
            </div>
          </div>
        </div>
        <div className="balances-card-chart">
          <SimpleAreaChart data={data} color={colorCharts} id={id} />
        </div>
      </Card>
    </div>
  );
};

const Balances = () => {
  const balances = useSelector(state => state.balances.items);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(balancesFetch());
  }, [dispatch])

  return (
    <div className="balances">
      <div className="balances-header">
        <TitleBlock>Balances</TitleBlock>
        <CustomSelect icon="calendar" onChange={(val) => dispatch(balancesUpdate(val))} />
      </div>

      <div className="balances-cards">
        {balances && balances.map((balance) => (
          <BalancesCard {...balance} key={balance.id} />
        ))}
      </div>
    </div>
  );
};

export default Balances;