import React, { useEffect } from 'react';
import { TitleBlock, Avatar, LinkBase } from 'components';
import { useSelector, useDispatch } from "react-redux";
import { recipientsFetch } from 'actions';

const Recipients = props => {
  const recipients = useSelector(state => state.recipients.items);
  const isFetching = useSelector(state => state.recipients.isFetching);
  const totalRecipients = useSelector(state => state.recipients.total);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(recipientsFetch());
  }, [dispatch]);

  return (
    <div className="recipients">
      <TitleBlock>Send money to</TitleBlock>

      <div className="recipients-list">
        <Avatar icon="add-user" />

        {isFetching && <div>Loading...</div>}

        {recipients &&
          recipients.map(recipient => (
            <Avatar img={recipient.img} key={recipient.id} />
          ))}

        {totalRecipients > recipients.length && (
          <LinkBase
            onClick={() => {
              console.log("load more recipients");
            }}
          >
            +{totalRecipients - recipients.length} more
          </LinkBase>
        )}
      </div>
    </div>
  );
};

export default Recipients;