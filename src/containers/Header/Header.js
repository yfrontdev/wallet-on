import React from 'react';
import { AppLogo, BtnSquare, Avatar, Icon, Dropdown, Burger } from 'components';
import { useDispatch, useSelector } from "react-redux";
import { addCustomMenuItem, toggleMobileMenu } from "actions";

const profileMenu = (
  <div className="menu profile-menu">
    <ul className="menu-list">
      <li className="menu-item"><a href="/" className="menu-link">Profile</a></li>
      <li className="menu-item"><a href="/" className="menu-link">Settings</a></li>
      <li className="menu-item"><a href="/" className="menu-link">Balances</a></li>
      <li className="menu-divider"></li>
      <li className="menu-item"><a href="/" className="menu-link">Exit</a></li>
    </ul>
  </div>
);

const Header = props => {
  const dispatch = useDispatch();
  const isOpenMobileMenu = useSelector(state => state.ui.isOpenMobileMenu);

  return (
    <div className="header">
      <div className="header-inner">
        <Burger
          isOpen={isOpenMobileMenu}
          onClick={() => dispatch(toggleMobileMenu())}
        />
        <AppLogo />
        <BtnSquare
          className="add-menu-item"
          icon="add"
          type="primary"
          onClick={() => dispatch(addCustomMenuItem())}
        />
        <BtnSquare icon="search" />
        <BtnSquare icon="bell" badge="4" />
        <Dropdown overlay={profileMenu} className="header-profile">
          <Avatar
            img="/api/image/recipients/user-00.png"
            size="md"
            hover={false}
          />
          <Icon name="arrow-down" />
        </Dropdown>
      </div>
    </div>
  );
};

export default Header;