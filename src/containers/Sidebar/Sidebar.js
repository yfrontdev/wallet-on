import React, { useEffect } from 'react';
import classNames from "classnames";
import { NavLink, Link } from 'react-router-dom';
import { Icon, AppLogo } from 'components';
import { menu } from 'utils/dummy-data/menu';
import { useSelector, useDispatch } from 'react-redux';
import { customMenuFetch, toggleMobileMenu } from "actions";

const SidebarMenu = () => {
  const notifications = useSelector(state => state.site.notifications);
  const customMenu = useSelector(state => state.site.customMenu);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(customMenuFetch());
  }, [dispatch])

  return (
    <ul className="sidebar-menu">
      {menu.map(({ link, label, icon }) => (
        <li className="sidebar-menu-item" key={link}>
          <NavLink to={link} exact className="sidebar-menu-item-link">
            <span className="sidebar-menu-item-icon">
              <Icon name={icon} />
            </span>
            <span className="sidebar-menu-item-label">{label}</span>
            {notifications[icon] && (
              <span className="sidebar-menu-item-badge">{notifications[icon]}</span>
            )}
          </NavLink>
        </li>
      ))}
      {customMenu &&
        customMenu.map((label, idx) => (
          <li className="sidebar-menu-item sidebar-menu-item--custom" key={idx}>
            <Link to="/" className="sidebar-menu-item-link" onClick={e => e.preventDefault()}>
              <span className="sidebar-menu-item-icon"></span>
              <span className="sidebar-menu-item-label">{label}</span>
            </Link>
          </li>
        ))}
    </ul>
  );
}

const Sidebar = props => {
  const isOpenMobileMenu = useSelector(state => state.ui.isOpenMobileMenu);
  const dispatch = useDispatch();

  return (
    <div
      className={classNames("sidebar", {
        "is-open": isOpenMobileMenu
      })}
    >
      <div className="sidebar-wrapper">
        <div className="sidebar-inner">
          <div className="sidebar-header">
            <AppLogo />
          </div>
          <SidebarMenu />
        </div>
      </div>
      <div
        className="sidebar-overlay"
        onClick={() => dispatch(toggleMobileMenu())}
      />
    </div>
  );
};

export default Sidebar;

