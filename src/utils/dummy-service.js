import { recipients } from './dummy-data/recipients';
import { mockBalances } from "./dummy-data/balances";

const dummyServices = {
  getBalances: (period) => {
    return mockBalances(period);
  },

  getInitialRecipients: () => {
    return recipients.slice(0, 8);
  },

  getAllRecipients: () => {
    return recipients;
  },

  getOverviewPage: () => {
    return {
      acitivity: this.getActivity(),
      balances: this.getBalances(),
      recipients: this.getRecipients()
    };
  }
};

const BASE_URL = 'http://www.mocky.io/v2/';

export const apiFetch = (path, realApi = true, { headers = {}, ...options } = {}, query = null) => (
  new Promise((resolve, reject) => {
    if (!realApi) {
      return resolve(dummyServices[path](query));
    }

    const url = `${BASE_URL}${path}`;
    return fetch(url, {
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        ...headers
      },
      ...options
    })
    .then(res => res.json())
    .then(data => resolve(data))
    .catch(err => {
      reject({ error: err });
    });
  })
)






