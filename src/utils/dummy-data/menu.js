export const menu = [
  {
    link: "/",
    label: "Overview",
    icon: "overview"
  },
  {
    link: "/activity",
    label: "Activity",
    icon: "activity"
  },
  {
    link: "/balances",
    label: "Balances",
    icon: "wallet"
  },
  {
    link: "/debit-cards",
    label: "Debit Cards",
    icon: "card"
  },
  {
    link: "/history",
    label: "History",
    icon: "progress"
  },
  {
    link: "/recipients",
    label: "Recipients",
    icon: "recipients"
  },
  {
    link: "/invite",
    label: "Invite",
    icon: "email"
  },
  {
    link: "/settings",
    label: "Settings",
    icon: "settings"
  }
];