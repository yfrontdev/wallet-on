const idx = (i) => {
  return i < 10 ? `0${i}` : i;
}

const mockRecipients = () => {
  const arr = [];
  for (let i = 1; i < 9; i++) {
    arr.push({
      id: `recipient-${idx(i)}`,
      img: `/api/image/recipients/user-${idx(i)}.png`
    });
  }

  return arr;
}

export const recipients = mockRecipients();