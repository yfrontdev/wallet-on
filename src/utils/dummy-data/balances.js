export const mockBalances = (period) => {
  const balances = [];

  for (let i = 1; i <= 3; i++) {
    balances.push(mockBalance(i, period));
  }

  return balances;
}

const mockBalance = (idx, period) => {
 const balance = {
   id: Math.floor(Math.random() * 16777215).toString(16),
   name: `DE${randomValue(10, 99, 0)} ${randomValue(1000, 4000, 0)} 0044 0532 0130 00`,
   diff: randomValue(0, 300, 0) - 100,
   data: [],
   colorCharts: randomColor(idx),
   period: `this ${period ? period : 'week'}`
 };

  let data = [];
  for (let i = 1; i < 11; i++) {
    let val = randomValue(1000, 3000)
    data.push({ name: `Day ${i}`, uv: val });
    if (i === 10) {
      balance.total = {
        value: val,
        currency: "eur"
      };
    }
  }
  balance.data = data;

  return balance;
}

const randomValue = (min, max, fix = 2) => {
  let rand = min + Math.random() * (max + 1 - min);
  // return Math.floor(rand);
  return rand.toFixed(fix);
};

const randomColor = (i) => {
  switch (i) {
    case 1:
      return "#5187E0";
    case 2:
      return "#39DABC";
    case 3:
      return "#B151E0";
    default:
      return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
  }
};