import React from 'react';
import { Recipients, Activity, Balances } from 'containers';
import { TitlePage, DebitCards } from 'components';

const PageOverview = props => {
  return (
    <div className="page page-overview">
      <div className="container">
        <div className="page-header">
          <TitlePage>Overview</TitlePage>
        </div>
        
        <div className="page-body">
          <Balances />

          <div className="row row-body">
            <div className="col-1">
              <DebitCards />
              <Recipients />
            </div>
            <div className="col-2">
              <Activity />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PageOverview;