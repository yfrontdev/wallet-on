import { BALANCES_FETCH } from './types';
import { apiFetch } from 'utils/dummy-service';

export const balancesFetch = () => ({
  type: BALANCES_FETCH,
  payload: apiFetch('getBalances', false)
});

export const balancesUpdate = (period) => ({
  type: BALANCES_FETCH,
  payload: apiFetch('getBalances', false, {}, period)
});