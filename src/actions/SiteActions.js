import { CUSTOM_MENU_FETCH, ADD_CUSTOM_MENU_ITEM } from "./types";

export const customMenuFetch = () => async (dispatch) => {
  const currentMenuItems = JSON.parse(localStorage.getItem("customMenu")) || [];
  dispatch({
    type: CUSTOM_MENU_FETCH,
    payload: currentMenuItems
  });
};

export const addCustomMenuItem = () => async (dispatch) => {
  const newMenuItems = await prompt("Add new menu item", "Menu item");
  let currentMenuItems = JSON.parse(localStorage.getItem("customMenu")) || [];
  currentMenuItems.push(newMenuItems);
  localStorage.setItem("customMenu", JSON.stringify(currentMenuItems));
  dispatch({ type: ADD_CUSTOM_MENU_ITEM, payload: newMenuItems });
};
