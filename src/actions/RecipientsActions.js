import { RECIPIENTS_FETCH } from './types';
import { apiFetch } from 'utils/dummy-service';

export const recipientsFetch = () => ({
  type: RECIPIENTS_FETCH,
  payload: apiFetch('getInitialRecipients', false)
});