import { ACTIVITIES_FETCH, ACTIVITIES_INIT } from "./types";
import { apiFetch } from 'utils/dummy-service';

export const activitiesInit = () => ({
  type: ACTIVITIES_INIT,
  payload: apiFetch("5e25ba362f00007400ce2c7c")
});

export const activitiesFetch = () => ({
  type: ACTIVITIES_FETCH,
  payload: apiFetch("5e25ba362f00007400ce2c7c?mocky-delay=1000ms")
});