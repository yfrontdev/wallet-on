import { MOBILE_MENU_TOGGLE } from './types';

export const toggleMobileMenu = () => ({
  type: MOBILE_MENU_TOGGLE
});